class User < ApplicationRecord
  has_secure_password

  has_many :borrows, dependent: :destroy
  has_many :borrowed_books, through: :borrows, source: :book

  validates :email, presence: true, uniqueness: true
  validates :password, presence: true, length: { minimum: 6 }, confirmation: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :phone, presence: true, format: { with: /\A\d{10}\z/, message: "must be 10 digits" }
  validates :date_of_birth, presence: true
  validates :membership_type, presence: true
  # validates :membership_expiry_date, presence: true
  validates :role, presence: true

  before_validation :set_default_role

  def full_name
    "#{first_name} #{last_name}"
  end

  private

  def set_default_role
    self.role ||= 'member'
    self.membership_type ||= 'standard'
    self.membership_expiry_date ||= Date.today.next_year
  end


end
