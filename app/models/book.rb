
class Book < ApplicationRecord
  has_many :borrows, dependent: :destroy
  has_many :users, through: :borrows
  validates :title, :author, :publish_year, presence: true

  def available?
    current_borrow.nil?
  end

  def current_borrow
    borrows.find_by(ended_at: nil)
  end

  def self.search(query)
    where("title LIKE ? OR author LIKE ?", "%#{query}%", "%#{query}%")
  end
end
