class Borrow < ApplicationRecord
  belongs_to :user
  belongs_to :book

  validates :user_id, :book_id, :started_at, presence: true
  validate :book_must_be_available, on: :create

  def book_must_be_available
    errors.add(:book, "is already borrowed") unless book.available?
  end
end
