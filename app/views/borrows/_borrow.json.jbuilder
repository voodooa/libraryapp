json.extract! borrow, :id, :user_id, :book_id, :started_at, :ended_at, :created_at, :updated_at
json.url borrow_url(borrow, format: :json)
