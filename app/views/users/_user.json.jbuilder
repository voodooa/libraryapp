json.extract! user, :id, :email, :password_digest, :first_name, :last_name, :address, :phone, :date_of_birth, :membership_type, :membership_expiry_date, :profile_picture, :joined_date, :role, :created_at, :updated_at
json.url user_url(user, format: :json)
