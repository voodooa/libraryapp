class BorrowsController < ApplicationController
  before_action :require_login
  before_action :set_borrow, only: [:destroy]
  before_action :authorize_user, only: [:destroy]

  def create
    @borrow = current_user.borrows.build(borrow_params)
    @borrow.started_at = Time.current
    if @borrow.save
      redirect_to dashboard_path, notice: 'Book borrowed successfully.'
    else
      redirect_to books_path, alert: 'Unable to borrow book.'
    end
  end

  def destroy
    if @borrow.update(ended_at: Time.current)
      redirect_to dashboard_path, notice: 'Book returned successfully.'
    else
      redirect_to dashboard_path, alert: 'Unable to return book.'
    end
  end

  private

  def set_borrow
    @borrow = Borrow.find(params[:id])
  end

  def authorize_user
    unless @borrow.user == current_user
      redirect_to dashboard_path, alert: 'You are not authorized to return this book.'
    end
  end

  def borrow_params
    params.permit(:book_id)
  end

  def require_login
    unless logged_in?
      redirect_to login_path, alert: 'You must be logged in to borrow a book.'
    end
  end
end
