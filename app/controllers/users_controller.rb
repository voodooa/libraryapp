class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :require_login, only: [:dashboard, :edit, :update, :destroy]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    @user.joined_date = Date.today # Set the joined date to today
    @user.role = 'member' # Set the default role to 'member'
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_path, notice: 'Account created successfully.'
    else
      render :new
    end
  end

  def show
    @user = current_user
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update(user_params)
      redirect_to @user, notice: 'Profile updated successfully.'
    else
      render :edit
    end
  end

  def destroy
    @user = current_user
    @user.destroy
    session[:user_id] = nil
    redirect_to root_path, notice: 'Account deleted successfully.'
  end

  # def dashboard
  #   @borrows = current_user.borrows.includes(:book)
  # end

  def dashboard
    @borrows = current_user.borrows.includes(:book)
    case params[:filter]
    when 'not_returned'
      @borrows = @borrows.where(ended_at: nil)
    when 'returned'
      @borrows = @borrows.where.not(ended_at: nil)
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name, :address, :phone, :date_of_birth, :membership_type, :profile_picture)
  end

  def require_login
    unless logged_in?
      redirect_to login_path, alert: 'You must be logged in to access this section.'
    end
  end
end
