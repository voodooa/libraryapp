# config/routes.rb

Rails.application.routes.draw do
  resources :books do
    member do
      get 'history'
    end
  end

  resources :borrows, only: [:create, :destroy]

  # Other routes...

  resources :users, only: [:new, :create, :show, :edit, :update, :destroy]
  get '/dashboard', to: 'users#dashboard', as: 'dashboard'

  get '/register', to: 'users#new', as: 'register'
  post '/users', to: 'users#create'
  get '/login', to: 'sessions#new', as: 'login'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy', as: 'logout'

  root 'books#index'
end
