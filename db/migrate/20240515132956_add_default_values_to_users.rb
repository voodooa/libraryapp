class AddDefaultValuesToUsers < ActiveRecord::Migration[6.1]
  def change
    change_column_default :users, :role, "member"
    change_column_default :users, :membership_type, "standard"
  end
end
