class ChangeMembershipExpiryDateToBeOptionalInUsers < ActiveRecord::Migration[7.1]
  def change
    change_column_null :users, :membership_expiry_date, true
  end
end
