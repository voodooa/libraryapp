class CreateUsers < ActiveRecord::Migration[7.1]
  def change
    create_table :users do |t|
      t.string :email
      t.string :password_digest
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :phone
      t.date :date_of_birth
      t.string :membership_type
      t.date :membership_expiry_date
      t.string :profile_picture
      t.date :joined_date
      t.string :role

      t.timestamps
    end
  end
end
