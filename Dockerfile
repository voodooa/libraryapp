# Dockerfile

# Make sure RUBY_VERSION matches the Ruby version in .ruby-version and Gemfile
ARG RUBY_VERSION=3.1.2
FROM registry.docker.com/library/ruby:$RUBY_VERSION-slim as base

# Rails app lives here
WORKDIR /rails

# Set production environment
ENV RAILS_ENV="development" \
    BUNDLE_DEPLOYMENT="1" \
    BUNDLE_PATH="/usr/local/bundle" \
    BUNDLE_WITHOUT="production"

# Install dependencies
RUN apt-get update -qq && \
    apt-get install --no-install-recommends -y \
    build-essential \
    libpq-dev \
    nodejs \
    yarn \
    sqlite3 \
    libsqlite3-dev

# Install gems
COPY Gemfile Gemfile.lock ./
RUN bundle install

# Copy the main application
COPY . .

# Precompile assets (if needed)
# RUN SECRET_KEY_BASE_DUMMY=1 bundle exec rails assets:precompile

# Set correct permissions for log and tmp directories
RUN mkdir -p log tmp && \
    chmod -R 777 log tmp

# Ensure that the log file exists
RUN touch log/development.log && \
    chmod 0664 log/development.log

# Ensure that the local_secret.txt file exists
RUN touch tmp/local_secret.txt && \
    chmod 0664 tmp/local_secret.txt

# Run as a non-root user for security
RUN useradd -ms /bin/bash appuser
USER appuser

# Entrypoint prepares the database
ENTRYPOINT ["bin/rails", "db:prepare"]

# Expose the port for the application
EXPOSE 3000

# Default command to run the server
CMD ["bin/rails", "server", "-b", "0.0.0.0"]
