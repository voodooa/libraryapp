# test/helpers/session_helpers.rb
module SessionHelpers
  def login_as(user)
    post login_url, params: { email: user.email, password: 'password' }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_equal user.id, session[:user_id]
  end
end
