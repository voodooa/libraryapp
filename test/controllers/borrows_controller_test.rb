require "test_helper"

class BorrowsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @borrow = borrows(:one)
    @book = books(:one)
    @user = users(:one)
    login_as(@user)
  end

  test "should create borrow" do
    assert_difference("Borrow.count") do
      post borrows_url, params: { book_id: @book.id }
    end

    assert_redirected_to dashboard_path
    follow_redirect!
  end

  test "should not create borrow for borrowed book" do
    # Mark the book as currently borrowed by setting ended_at to nil
    @borrow.update(ended_at: nil)
    assert_no_difference("Borrow.count") do
      post borrows_url, params: { book_id: @book.id }
    end

    assert_redirected_to books_path
    follow_redirect!
  end


  test "should not destroy borrow not owned by user" do
    @other_user = users(:two)
    @borrow.update(user: @other_user)

    assert_no_difference("Borrow.count") do
      delete borrow_url(@borrow)
    end

    assert_redirected_to dashboard_path
    follow_redirect!
  end
end
