require "test_helper"

class UsersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user = users(:one)
    @other_user = users(:two)
  end

  test "should get new" do
    get new_user_url
    assert_response :success
  end

  test "should create user" do
    assert_difference('User.count') do
      post users_url, params: { user: { email: 'newuser@example.com', password: 'password', password_confirmation: 'password', first_name: 'New', last_name: 'User', address: '789 Maple St', phone: '5555555555', date_of_birth: '2000-01-01', membership_type: 'regular', role: 'member' } }
    end
    assert_redirected_to root_url
  end

  test "should show user" do
    login_as(@user)
    get user_url(@user)
    assert_response :success
  end

  test "should get edit" do
    login_as(@user)
    get edit_user_url(@user)
    assert_response :success
  end

  test "should update user" do
    login_as(@user)
    patch user_url(@user), params: { user: { email: @user.email, password: 'password', password_confirmation: 'password', first_name: 'Updated', last_name: 'Name', address: @user.address, phone: @user.phone, date_of_birth: @user.date_of_birth, membership_type: @user.membership_type, role: @user.role } }
    assert_redirected_to user_url(@user)
  end

  test "should destroy user" do
    login_as(@user)
    assert_difference('User.count', -1) do
      delete user_url(@user)
    end
    assert_redirected_to root_url
  end

  test "should get dashboard" do
    login_as(@user)
    get dashboard_url
    assert_response :success
  end
end
