# ENV["RAILS_ENV"] ||= "test"
# require_relative "../config/environment"
# require "rails/test_help"
#
# module ActiveSupport
#   class TestCase
#     # Run tests in parallel with specified workers
#     parallelize(workers: :number_of_processors)
#
#     # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
#     fixtures :all
#
#     # Add more helper methods to be used by all tests here...
#   end
# end

# test/test_helper.rb
ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
require 'rails/test_help'

module SessionHelpers
  def login_as(user)
    post login_url, params: { email: user.email, password: 'password' }
    assert_response :redirect
    follow_redirect!
    assert_response :success
    assert_equal user.id, session[:user_id]
  end
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Add more helper methods to be used by all tests here...
  include SessionHelpers
end
