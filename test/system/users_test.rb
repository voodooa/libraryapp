require "application_system_test_case"

class UsersTest < ApplicationSystemTestCase
  setup do
    @user = users(:one)
  end

  test "visiting the index" do
    visit users_url
    assert_selector "h1", text: "Users"
  end

  test "should create user" do
    visit users_url
    click_on "New user"

    fill_in "Address", with: @user.address
    fill_in "Date of birth", with: @user.date_of_birth
    fill_in "Email", with: @user.email
    fill_in "First name", with: @user.first_name
    fill_in "Joined date", with: @user.joined_date
    fill_in "Last name", with: @user.last_name
    fill_in "Membership expiry date", with: @user.membership_expiry_date
    fill_in "Membership type", with: @user.membership_type
    fill_in "Password digest", with: @user.password_digest
    fill_in "Phone", with: @user.phone
    fill_in "Profile picture", with: @user.profile_picture
    fill_in "Role", with: @user.role
    click_on "Create User"

    assert_text "User was successfully created"
    click_on "Back"
  end

  test "should update User" do
    visit user_url(@user)
    click_on "Edit this user", match: :first

    fill_in "Address", with: @user.address
    fill_in "Date of birth", with: @user.date_of_birth
    fill_in "Email", with: @user.email
    fill_in "First name", with: @user.first_name
    fill_in "Joined date", with: @user.joined_date
    fill_in "Last name", with: @user.last_name
    fill_in "Membership expiry date", with: @user.membership_expiry_date
    fill_in "Membership type", with: @user.membership_type
    fill_in "Password digest", with: @user.password_digest
    fill_in "Phone", with: @user.phone
    fill_in "Profile picture", with: @user.profile_picture
    fill_in "Role", with: @user.role
    click_on "Update User"

    assert_text "User was successfully updated"
    click_on "Back"
  end

  test "should destroy User" do
    visit user_url(@user)
    click_on "Destroy this user", match: :first

    assert_text "User was successfully destroyed"
  end
end
